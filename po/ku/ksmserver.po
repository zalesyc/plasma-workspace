# translation of ksmserver.po to Kurdish
# Kurdish translation for kdebase
# Copyright (c) (c) 2006 Canonical Ltd, and Rosetta Contributors 2006
# This file is distributed under the same license as the kdebase package.
#
# FIRST AUTHOR <EMAIL@ADDRESS>, 2006.
# Erdal Ronahi <erdal.ronahi@nospam.gmail.com>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: ksmserver\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-27 01:41+0000\n"
"PO-Revision-Date: 2008-12-23 03:34+0200\n"
"Last-Translator: Omer Ensari <oensari@gmail.com>\n"
"Language-Team: Kurdish <ku@li.org>\n"
"Language: ku\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2007-11-26 09:56+0000\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: logout.cpp:340
#, kde-format
msgid "Logout canceled by '%1'"
msgstr "Derketin ji hêla '%1' hat betalkirin"

#: main.cpp:74
#, kde-format
msgid "$HOME not set!"
msgstr ""

#: main.cpp:78 main.cpp:86
#, kde-format
msgid "$HOME directory (%1) does not exist."
msgstr ""

#: main.cpp:81
#, kde-kuit-format
msgctxt "@info"
msgid ""
"No write access to $HOME directory (%1). If this is intentional, set "
"<envar>KDE_HOME_READONLY=1</envar> in your environment."
msgstr ""

#: main.cpp:88
#, kde-format
msgid "No read access to $HOME directory (%1)."
msgstr ""

#: main.cpp:92
#, kde-format
msgid "$HOME directory (%1) is out of disk space."
msgstr ""

#: main.cpp:95
#, kde-format
msgid "Writing to the $HOME directory (%2) failed with the error '%1'"
msgstr ""

#: main.cpp:108 main.cpp:143
#, kde-format
msgid "No write access to '%1'."
msgstr ""

#: main.cpp:110 main.cpp:145
#, kde-format
msgid "No read access to '%1'."
msgstr ""

#: main.cpp:118 main.cpp:131
#, kde-format
msgid "Temp directory (%1) is out of disk space."
msgstr ""

#: main.cpp:121 main.cpp:134
#, kde-format
msgid ""
"Writing to the temp directory (%2) failed with\n"
"    the error '%1'"
msgstr ""

#: main.cpp:149
#, kde-format
msgid ""
"The following installation problem was detected\n"
"while trying to start Plasma:"
msgstr ""

#: main.cpp:152
#, kde-format
msgid ""
"\n"
"\n"
"Plasma is unable to start.\n"
msgstr ""

#: main.cpp:159
#, kde-format
msgid "Plasma Workspace installation problem!"
msgstr ""

#: main.cpp:193
#, fuzzy, kde-format
#| msgid ""
#| "The reliable KDE session manager that talks the standard X11R6 \n"
#| "session management protocol (XSMP)."
msgid ""
"The reliable Plasma session manager that talks the standard X11R6 \n"
"session management protocol (XSMP)."
msgstr ""
"Rêveberê danişîna KDE yê ewle ku protokola (XSMP) rêveberiya danişînê ya "
"X11R6 a Standart bi kar tîne."

#: main.cpp:197
#, kde-format
msgid "Restores the saved user session if available"
msgstr "Heke pêkan be danişîna tomarkirî ji nû ve lê bar dike"

#: main.cpp:200
#, kde-format
msgid "Also allow remote connections"
msgstr "Destûrê bide pêwendiya ji dûr ve"

#: main.cpp:203
#, kde-format
msgid "Starts the session in locked mode"
msgstr ""

#: main.cpp:207
#, kde-format
msgid ""
"Starts without lock screen support. Only needed if other component provides "
"the lock screen."
msgstr ""

#: server.cpp:885
#, fuzzy, kde-format
#| msgid "The KDE Session Manager"
msgid "Session Management"
msgstr "Rêveberê danişîna KDE"

#: server.cpp:890
#, fuzzy, kde-format
#| msgid "&Logout"
msgid "Log Out"
msgstr "&Derkeve"

#: server.cpp:895
#, kde-format
msgid "Shut Down"
msgstr ""

#: server.cpp:900
#, kde-format
msgid "Reboot"
msgstr ""

#: server.cpp:906
#, kde-format
msgid "Log Out Without Confirmation"
msgstr ""

#: server.cpp:911
#, kde-format
msgid "Shut Down Without Confirmation"
msgstr ""

#: server.cpp:916
#, kde-format
msgid "Reboot Without Confirmation"
msgstr ""

#, fuzzy
#~| msgid ""
#~| "Starts 'wm' in case no other window manager is \n"
#~| "participating in the session. Default is 'kwin'"
#~ msgid ""
#~ "Starts <wm> in case no other window manager is \n"
#~ "participating in the session. Default is 'kwin'"
#~ msgstr ""
#~ "Heke rêvebereke din a paceyê di danişînê de tune be 'wm'ê dide "
#~ "destpêkirin. 'kwin'a bi pêşdanasîn."

#, fuzzy
#~| msgid "&Logout"
#~ msgid "Logout"
#~ msgstr "&Derkeve"

#, fuzzy
#~| msgid "Logging out in 1 second."
#~| msgid_plural "Logging out in %1 seconds."
#~ msgid "Sleeping in 1 second"
#~ msgid_plural "Sleeping in %1 seconds"
#~ msgstr[0] "Wê 1 çirke de derkeve."
#~ msgstr[1] "Wê %1 çirkeyan de derkeve."

#~ msgid "Logging out in 1 second."
#~ msgid_plural "Logging out in %1 seconds."
#~ msgstr[0] "Wê 1 çirke de derkeve."
#~ msgstr[1] "Wê %1 çirkeyan de derkeve."

#~ msgid "Turning off computer in 1 second."
#~ msgid_plural "Turning off computer in %1 seconds."
#~ msgstr[0] "Komputer wê 1 çirke paşê dade."
#~ msgstr[1] "Komputer wê %1 çirkeyan paşê dade."

#~ msgid "Restarting computer in 1 second."
#~ msgid_plural "Restarting computer in %1 seconds."
#~ msgstr[0] "Komputer wê 1 çirke din paşê ji nû ve destpê bike. "
#~ msgstr[1] "Komputer wê %1 çirkeyan paşê ji nû ve destpê bike. "

#, fuzzy
#~| msgid "&Turn Off Computer"
#~ msgid "Turn Off Computer"
#~ msgstr "&Komputerê Dade"

#, fuzzy
#~| msgid "&Restart Computer"
#~ msgid "Restart Computer"
#~ msgstr "Kompûterê ji nû ve bide &destpê kirin"

#, fuzzy
#~| msgid "&Cancel"
#~ msgid "Cancel"
#~ msgstr "&Betal"

#~ msgid "&Standby"
#~ msgstr "&Bendêman"

#~ msgid "Suspend to &RAM"
#~ msgstr "Ji &RAM ê Bisekine"

#~ msgid "Suspend to &Disk"
#~ msgstr "Ji &Dîsk ê Bisekine"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Erdal Ronahi"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "erdal.ronahi@nospam.gmail.com"

#~ msgid "(C) 2000, The KDE Developers"
#~ msgstr "(C) 2000, Pêşdebirên KDE"

#~ msgid "Matthias Ettrich"
#~ msgstr "Matthias Ettrich"

#~ msgid "Luboš Luňák"
#~ msgstr "Luboš Luňák"

#~ msgid "Maintainer"
#~ msgstr "Kesê miqet"

#~ msgctxt "current option in boot loader"
#~ msgid " (current)"
#~ msgstr " (aniha)"

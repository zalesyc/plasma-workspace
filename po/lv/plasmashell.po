# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Maris Nartiss <maris.kde@gmail.com>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-27 01:41+0000\n"
"PO-Revision-Date: 2020-07-31 11:04+0300\n"
"Last-Translator: Maris Nartiss <maris.kde@gmail.com>\n"
"Language-Team: Latvian <kde-i18n-doc@kde.org>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : "
"2);\n"
"X-Generator: Lokalize 20.04.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Māris Nartišs"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "maris.kde@gmail.com"

#: currentcontainmentactionsmodel.cpp:204
#, kde-format
msgid "Configure Mouse Actions Plugin"
msgstr "Konfigurēt peles darbību spraudni"

#: desktopview.cpp:225
#, fuzzy, kde-format
#| msgid "Plasma"
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1 Dev"
msgstr "Plasma"

#: desktopview.cpp:229
#, fuzzy, kde-format
#| msgid "Plasma"
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1 Beta"
msgstr "Plasma"

#: desktopview.cpp:232
#, kde-format
msgctxt "@label %1 is the Plasma version, %2 is the beta release number"
msgid "KDE Plasma %1 Beta %2"
msgstr ""

#: desktopview.cpp:236
#, fuzzy, kde-format
#| msgid "Plasma"
msgctxt "@label %1 is the Plasma version"
msgid "KDE Plasma %1"
msgstr "Plasma"

#: desktopview.cpp:242
#, kde-format
msgctxt "@info:usagetip"
msgid "Visit bugs.kde.org to report issues"
msgstr ""

#: main.cpp:110
#, kde-format
msgid "Plasma Shell"
msgstr "Plasma čaula"

#: main.cpp:122
#, kde-format
msgid "Enable QML Javascript debugger"
msgstr "Ieslēgt QML Javascript atkļūdotāju"

#: main.cpp:125
#, kde-format
msgid "Do not restart plasma-shell automatically after a crash"
msgstr "Nepārstartēt automātiski Plasma čaulu pēc avārijas"

#: main.cpp:128
#, kde-format
msgid "Force loading the given shell plugin"
msgstr "Ielādēt norādīto čaulas spraudni piespiedu kārtā"

#: main.cpp:132
#, kde-format
msgid "Replace an existing instance"
msgstr "Aizvietot jau eksistējošu"

#: main.cpp:135
#, kde-format
msgid ""
"Enables test mode and specifies the layout javascript file to set up the "
"testing environment"
msgstr ""
"Ieslēdz testa režīmu un norāda Javascript datni, kas iestata testa vidi"

#: main.cpp:136
#, kde-format
msgid "file"
msgstr "datne"

#: main.cpp:140
#, kde-format
msgid "Lists the available options for user feedback"
msgstr "Lietotājam pieejamo atgriezeniskās saites opciju saraksts"

#: main.cpp:222
#, kde-format
msgid "Plasma Failed To Start"
msgstr "Plasma nepalaidās"

#: main.cpp:223
#, kde-format
msgid ""
"Plasma is unable to start as it could not correctly use OpenGL 2 or software "
"fallback\n"
"Please check that your graphic drivers are set up correctly."
msgstr ""
"Plasma nespēja palaisties, jo nebija iespējams korekti izmantot OpenGL 2 vai "
"programmatūras versiju Pārbaudiet, vai grafiskās kartes dzinēji ir korekti "
"iestatīti."

#: osd.cpp:53
#, kde-format
msgctxt "OSD informing that the system is muted, keep short"
msgid "Audio Muted"
msgstr "Skaņa ir izslēgta"

#: osd.cpp:75
#, kde-format
msgctxt "OSD informing that the microphone is muted, keep short"
msgid "Microphone Muted"
msgstr "Mikrofons ir izslēgts"

#: osd.cpp:91
#, kde-format
msgctxt "OSD informing that some media app is muted, eg. Amarok Muted"
msgid "%1 Muted"
msgstr "%1 skaņa ir izslēgta"

#: osd.cpp:113
#, kde-format
msgctxt "touchpad was enabled, keep short"
msgid "Touchpad On"
msgstr "Skārienpaliktnis ir ieslēgts"

#: osd.cpp:115
#, kde-format
msgctxt "touchpad was disabled, keep short"
msgid "Touchpad Off"
msgstr "Skārienpaliknis ir izslēgts"

#: osd.cpp:122
#, kde-format
msgctxt "wireless lan was enabled, keep short"
msgid "Wifi On"
msgstr "Wifi ir ieslēgts"

#: osd.cpp:124
#, kde-format
msgctxt "wireless lan was disabled, keep short"
msgid "Wifi Off"
msgstr "Wifi ir izslēgts"

#: osd.cpp:131
#, kde-format
msgctxt "Bluetooth was enabled, keep short"
msgid "Bluetooth On"
msgstr "Bluetooth ir ieslēgts"

#: osd.cpp:133
#, kde-format
msgctxt "Bluetooth was disabled, keep short"
msgid "Bluetooth Off"
msgstr "Bluetooth ir izslēgts"

#: osd.cpp:140
#, kde-format
msgctxt "mobile internet was enabled, keep short"
msgid "Mobile Internet On"
msgstr "Mobilais internets ir ieslēgts"

#: osd.cpp:142
#, kde-format
msgctxt "mobile internet was disabled, keep short"
msgid "Mobile Internet Off"
msgstr "Mobilais internets ir izslēgts"

#: osd.cpp:150
#, kde-format
msgctxt ""
"on screen keyboard was enabled because physical keyboard got unplugged, keep "
"short"
msgid "On-Screen Keyboard Activated"
msgstr "Aktivēta uz ekrāna redzama tastatūra"

#: osd.cpp:153
#, kde-format
msgctxt ""
"on screen keyboard was disabled because physical keyboard was plugged in, "
"keep short"
msgid "On-Screen Keyboard Deactivated"
msgstr "Deaktivēta uz ekrāna redzama tastatūra"

#: shellcontainmentconfig.cpp:105
#, kde-format
msgid "Internal Screen on %1"
msgstr ""

#: shellcontainmentconfig.cpp:109
#, kde-format
msgctxt "Screen manufacturer and model on connector"
msgid "%1 %2 on %3"
msgstr ""

#: shellcontainmentconfig.cpp:133
#, kde-format
msgid "Disconnected Screen %1"
msgstr ""

#: shellcorona.cpp:174 shellcorona.cpp:176
#, kde-format
msgid "Show Desktop"
msgstr "Parādīt darbvirsmu"

#: shellcorona.cpp:176
#, kde-format
msgid "Hide Desktop"
msgstr "Slēpt darbvirsmu"

#: shellcorona.cpp:192
#, kde-format
msgid "Show Activity Switcher"
msgstr ""

#: shellcorona.cpp:203
#, kde-format
msgid "Stop Current Activity"
msgstr "Pārtraukt pašreizējo aktivitāti"

#: shellcorona.cpp:211
#, kde-format
msgid "Switch to Previous Activity"
msgstr "Pārslēgties uz iepriekšējo aktivitāti"

#: shellcorona.cpp:219
#, kde-format
msgid "Switch to Next Activity"
msgstr "Pārslēgties uz nākamo aktivitāti"

#: shellcorona.cpp:234
#, kde-format
msgid "Activate Task Manager Entry %1"
msgstr "Aktivēt uzdevumu pārvaldnieka vienumu %1"

#: shellcorona.cpp:261
#, kde-format
msgid "Manage Desktops And Panels..."
msgstr ""

#: shellcorona.cpp:283
#, kde-format
msgid "Move keyboard focus between panels"
msgstr ""

#: shellcorona.cpp:1999
#, kde-format
msgid "Add Panel"
msgstr "Pievienot paneli"

#: shellcorona.cpp:2035
#, fuzzy, kde-format
#| msgid "Empty %1"
msgctxt "Creates an empty containment (%1 is the containment name)"
msgid "Empty %1"
msgstr "Tukšs %1"

#: softwarerendernotifier.cpp:27
#, kde-format
msgid "Software Renderer In Use"
msgstr "Tiek izmantots programmatūras renderētājs"

#: softwarerendernotifier.cpp:28
#, kde-format
msgctxt "Tooltip telling user their GL drivers are broken"
msgid "Software Renderer In Use"
msgstr "Tiek izmantots programmatūras renderētājs"

#: softwarerendernotifier.cpp:29
#, kde-format
msgctxt "Tooltip telling user their GL drivers are broken"
msgid "Rendering may be degraded"
msgstr "Renderēšana var būt degradēta"

#: softwarerendernotifier.cpp:39
#, kde-format
msgid "Never show again"
msgstr "Turpmāk nerādīt"

#: userfeedback.cpp:36
#, kde-format
msgid "Panel Count"
msgstr "Paneļu skaits"

#: userfeedback.cpp:40
#, kde-format
msgid "Counts the panels"
msgstr "Saskaita paneļus"

#~ msgid ""
#~ "Load plasmashell as a standalone application, needs the shell-plugin "
#~ "option to be specified"
#~ msgstr ""
#~ "Ielādēt Plasma čaulu kā atsevišķu programmu. Nepieciešams norādīt čaulas "
#~ "spraudņu opciju"

#~ msgid "Unable to load script file: %1"
#~ msgstr "Nebija iespējams ielādēt skripta datni: %1"

#~ msgid "Activities..."
#~ msgstr "Aktivitātes..."

# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-03 02:20+0000\n"
"PO-Revision-Date: 2023-10-04 21:53+0300\n"
"Last-Translator: \n"
"Language-Team: Hebrew <kde-i18n-doc@kde.org>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n == 1) ? 0 : ((n == 2) ? 1 : ((n > 10 && "
"n % 10 == 0) ? 2 : 3));\n"
"X-Generator: Poedit 3.3.2\n"

#: ui/LocationsFixedView.qml:39
#, kde-format
msgctxt ""
"@label:chooser Tap should be translated to mean touching using a touchscreen"
msgid "Tap to choose your location on the map."
msgstr "נא לגעת כדי לבחור את המקום שלך במפה."

#: ui/LocationsFixedView.qml:40
#, kde-format
msgctxt ""
"@label:chooser Click should be translated to mean clicking using a mouse"
msgid "Click to choose your location on the map."
msgstr "יש ללחוץ כדי לבחור את המקום שלך במפה."

#: ui/LocationsFixedView.qml:78 ui/LocationsFixedView.qml:103
#, kde-format
msgid "Zoom in"
msgstr "התקרבות"

#: ui/LocationsFixedView.qml:222
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Modified from <link url='https://commons.wikimedia.org/wiki/File:"
"World_location_map_(equirectangular_180).svg'>World location map</link> by "
"TUBS / Wikimedia Commons / <link url='https://creativecommons.org/licenses/"
"by-sa/3.0'>CC BY-SA 3.0</link>"
msgstr ""
"השתנה מתוך <link url='https://commons.wikimedia.org/wiki/File:"
"World_location_map_(equirectangular_180).svg'>מפת המקומות העולמית</link> מאת "
"TUBS / ויקישיתוף / <link url='https://creativecommons.org/licenses/by-"
"sa/3.0'>קריאייטיב קומונס ייחוס-שיתוף דומה 3.0</link>"

#: ui/LocationsFixedView.qml:235
#, kde-format
msgctxt "@label: textbox"
msgid "Latitude:"
msgstr "קו רוחב:"

#: ui/LocationsFixedView.qml:261
#, kde-format
msgctxt "@label: textbox"
msgid "Longitude:"
msgstr "קו גובה:"

#: ui/main.qml:87
#, kde-format
msgid "The blue light filter makes the colors on the screen warmer."
msgstr "מסנן הצבע הכחול מחמם את צבעי המסך."

#: ui/main.qml:98
#, kde-format
msgid "Switching times:"
msgstr "שעות מעבר:"

#: ui/main.qml:100
#, kde-format
msgid "Always off"
msgstr "כבוי תמיד"

#: ui/main.qml:101
#, kde-format
msgid "Sunset and sunrise at current location"
msgstr "מועדי זריחה ושקיעה במקום הנוכחי"

#: ui/main.qml:102
#, kde-format
msgid "Sunset and sunrise at manual location"
msgstr "מועדי זריחה ושקיעה במקום ידני"

#: ui/main.qml:103
#, kde-format
msgid "Custom times"
msgstr "מועדים בהתאמה אישית"

#: ui/main.qml:104
#, kde-format
msgid "Always on night color"
msgstr "תמיד צבע לילה"

#: ui/main.qml:156
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The device's location will be periodically updated using GPS (if available), "
"or by sending network information to <link url='https://location.services."
"mozilla.com'>Mozilla Location Service</link>."
msgstr ""
"מקום ההתקן יעודכן באופן תדיר דרך הנווטן (אם יש) או על ידי שליחת פרטי הרשת אל "
"<link url='https://location.services.mozilla.com'>שירות המיקום של מוזילה</"
"link>."

#: ui/main.qml:175
#, kde-format
msgid "Day color temperature:"
msgstr "טמפרטורת צבע יום:"

#: ui/main.qml:218 ui/main.qml:277
#, kde-format
msgctxt "Color temperature in Kelvin"
msgid "%1K"
msgstr "%1K"

#: ui/main.qml:222 ui/main.qml:281
#, kde-format
msgctxt "Night colour red-ish"
msgid "Warm"
msgstr "חמים"

#: ui/main.qml:228 ui/main.qml:287
#, kde-format
msgctxt "No blue light filter activated"
msgid "Cool (no filter)"
msgstr "קריר (ללא מסנן)"

#: ui/main.qml:228 ui/main.qml:287
#, kde-format
msgctxt "Night colour blue-ish"
msgid "Cool"
msgstr "קריר"

#: ui/main.qml:234
#, kde-format
msgid "Night color temperature:"
msgstr "טמפרטורת צבע לילה:"

#: ui/main.qml:298
#, kde-format
msgid "Latitude: %1°   Longitude: %2°"
msgstr "קו רוחב: %1°   קו גובה: %2°"

#: ui/main.qml:308
#, kde-format
msgid "Begin night color at:"
msgstr "להתחיל צבע לילה ב־:"

#: ui/main.qml:321 ui/main.qml:344
#, kde-format
msgid "Input format: HH:MM"
msgstr "תבנית קלט: HH:MM"

#: ui/main.qml:331
#, kde-format
msgid "Begin day color at:"
msgstr "להתחיל את צבע היום ב־:"

#: ui/main.qml:353
#, kde-format
msgid "Transition duration:"
msgstr "משך מעבר:"

#: ui/main.qml:362
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "דקה"
msgstr[1] "%1 דקות"
msgstr[2] "%1 דקות"
msgstr[3] "%1 דקות"

#: ui/main.qml:375
#, kde-format
msgid "Input minutes - min. 1, max. 600"
msgstr "קלט דקות - מ־1 עד 600"

#: ui/main.qml:393
#, kde-format
msgid "Error: Transition time overlaps."
msgstr "שגיאה: זמני המעבר חופפים."

#: ui/main.qml:415
#, kde-format
msgctxt "@info:placeholder"
msgid "Locating…"
msgstr "מתבצע איתור…"

#: ui/TimingsView.qml:32
#, kde-format
msgid ""
"Color temperature begins changing to night time at %1 and is fully changed "
"by %2"
msgstr ""
"טמפרטורת הצבע מתחילה להשתנות לתאורת לילה ב־%1 והיא מסיימת את השינוי ב־%2"

#: ui/TimingsView.qml:39
#, kde-format
msgid ""
"Color temperature begins changing to day time at %1 and is fully changed by "
"%2"
msgstr ""
"טמפרטורת הצבע מתחילה להשתנות לתאורת יום ב־%1 והיא מסיימת את השינוי ב־%2"

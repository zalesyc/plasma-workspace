# translation of plasma_applet_devicenotifier.po to Icelandic
# Copyright (C) 2008 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Sveinn í Felli <sveinki@nett.is>, 2008, 2009, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_devicenotifier\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-28 01:41+0000\n"
"PO-Revision-Date: 2022-08-24 14:58+0000\n"
"Last-Translator: Sveinn í Felli <sv1@fellsnet.is>\n"
"Language-Team: Icelandic\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"\n"
"\n"
"X-Generator: Lokalize 21.12.3\n"

#: package/contents/ui/DeviceItem.qml:185
#, kde-format
msgctxt "@info:status Free disk space"
msgid "%1 free of %2"
msgstr "%1 laust af %2"

#: package/contents/ui/DeviceItem.qml:189
#, kde-format
msgctxt ""
"Accessing is a less technical word for Mounting; translation should be short "
"and mean 'Currently mounting this device'"
msgid "Accessing…"
msgstr "Fæ aðgang…"

#: package/contents/ui/DeviceItem.qml:192
#, kde-format
msgctxt ""
"Removing is a less technical word for Unmounting; translation should be "
"short and mean 'Currently unmounting this device'"
msgid "Removing…"
msgstr "Fjarlægi..."

#: package/contents/ui/DeviceItem.qml:195
#, kde-format
msgid "Don't unplug yet! Files are still being transferred..."
msgstr "Ekki rífa strax úr sambandi! Enn er verið að flytja skrár..."

#: package/contents/ui/DeviceItem.qml:226
#, kde-format
msgid "Open in File Manager"
msgstr "Opna í skráastjóra"

#: package/contents/ui/DeviceItem.qml:229
#, kde-format
msgid "Mount and Open"
msgstr "Tengja og opna"

#: package/contents/ui/DeviceItem.qml:231
#, kde-format
msgid "Eject"
msgstr "Henda út"

#: package/contents/ui/DeviceItem.qml:233
#, kde-format
msgid "Safely remove"
msgstr "Fjarlæga á öruggan máta"

#: package/contents/ui/DeviceItem.qml:275
#, kde-format
msgid "Mount"
msgstr "Tengja í skráakerfið"

#: package/contents/ui/FullRepresentation.qml:43
#: package/contents/ui/main.qml:252
#, kde-format
msgid "Remove All"
msgstr "Fjarlægja allt"

#: package/contents/ui/FullRepresentation.qml:44
#, kde-format
msgid "Click to safely remove all devices"
msgstr "Smelltu til að fjarlæga öll tæki á öruggan máta"

#: package/contents/ui/FullRepresentation.qml:185
#, kde-format
msgid "No removable devices attached"
msgstr "Engin útskiptanleg tæki tengd"

#: package/contents/ui/FullRepresentation.qml:185
#, kde-format
msgid "No disks available"
msgstr "Engir diskar tiltækir"

#: package/contents/ui/main.qml:56
#, kde-format
msgid "Most Recent Device"
msgstr "Nýlegasta tæki"

#: package/contents/ui/main.qml:56
#, kde-format
msgid "No Devices Available"
msgstr "Engin tæki tiltæk"

#: package/contents/ui/main.qml:234
#, kde-format
msgctxt "Open auto mounter kcm"
msgid "Configure Removable Devices…"
msgstr "Stilla útskiptanleg tæki…"

#: package/contents/ui/main.qml:260
#, kde-format
msgid "Removable Devices"
msgstr "Útskiptanleg tæki"

#: package/contents/ui/main.qml:275
#, kde-format
msgid "Non Removable Devices"
msgstr "Ekki-útskiptanleg tæki"

#: package/contents/ui/main.qml:290
#, kde-format
msgid "All Devices"
msgstr "Öll tæki"

#: package/contents/ui/main.qml:307
#, kde-format
msgid "Show popup when new device is plugged in"
msgstr "Birta sprettglugga þegar ný tæki eru tengd"

#, fuzzy
#~| msgid "General"
#~ msgid "General"
#~ msgstr "Almennt"

#, fuzzy
#~| msgid "1 action for this device"
#~| msgid_plural "%1 actions for this device"
#~ msgid "It is currently safe to remove this device."
#~ msgstr "1 aðgerð fyrir þetta tæki"

#, fuzzy
#~| msgid "1 action for this device"
#~| msgid_plural "%1 actions for this device"
#~ msgid "1 action for this device"
#~ msgid_plural "%1 actions for this device"
#~ msgstr[0] "1 aðgerð fyrir þetta tæki"
#~ msgstr[1] "1 aðgerð fyrir þetta tæki"

#, fuzzy
#~| msgid "1 action for this device"
#~| msgid_plural "%1 actions for this device"
#~ msgid "Click to eject this disc."
#~ msgstr "1 aðgerð fyrir þetta tæki"

#, fuzzy
#~| msgid "1 action for this device"
#~| msgid_plural "%1 actions for this device"
#~ msgid "Click to safely remove this device."
#~ msgstr "1 aðgerð fyrir þetta tæki"

#, fuzzy
#~| msgid "1 action for this device"
#~| msgid_plural "%1 actions for this device"
#~ msgid "Click to mount this device."
#~ msgstr "1 aðgerð fyrir þetta tæki"

#, fuzzy
#~| msgid "Hide "
#~ msgctxt "Hide a device"
#~ msgid "Hide %1"
#~ msgstr "Fela "

#, fuzzy
#~| msgid ""
#~| "Could not unmount the device.\n"
#~| "One or more files on this device are open within an application."
#~ msgid ""
#~ "Could not unmount device %1.\n"
#~ "One or more files on this device are open within an application."
#~ msgstr ""
#~ "Get ekki aftengt tækið.\n"
#~ "Ein eða fleiri skrár á þessu tæki eru opnar í forritum."

#~ msgid ""
#~ "Cannot eject the disc.\n"
#~ "One or more files on this disc are open within an application."
#~ msgstr ""
#~ "Get ekki spýtt út disknum.\n"
#~ "Ein eða fleiri skrár á disknum eru opnar í forritum."

#, fuzzy
#~| msgid "Last plugged in device: %1"
#~ msgid "Could not mount device %1."
#~ msgstr "Síðasta tengda tæki: %1"

#~ msgid "Cannot mount the disc."
#~ msgstr "Get ekki tengt diskinn."

#, fuzzy
#~| msgid "<font color=\"%1\">Devices recently plugged in:</font>"
#~ msgid "Devices recently plugged in:"
#~ msgstr "<font color=\"%1\">Nýlega tengd tæki:</font>"

#~ msgid " second"
#~ msgid_plural " seconds"
#~ msgstr[0] " sekúnda"
#~ msgstr[1] " sekúndur"

#~ msgid "Show all the items"
#~ msgstr "Sýna alla hluti"

#~ msgid "never"
#~ msgstr "aldrei"

#~ msgid "Form"
#~ msgstr "Form"
